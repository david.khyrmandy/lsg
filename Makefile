OBJS = src/lsg_main.o \
       src/lsg_match.o \
       src/lsg_ref.o \
       src/lsg_render.o \
       src/intset.o \
       src/strutil.o \
       data/lsg_data.o
CFLAGS += -Wall -Isrc/
LDLIBS += -lreadline

lsg: $(OBJS)
	$(CC) -o $@ $(LDFLAGS) $(OBJS) $(LDLIBS)

src/lsg_main.o: src/lsg_main.c src/lsg_config.h src/lsg_data.h src/lsg_match.h src/lsg_ref.h src/lsg_render.h src/strutil.h

src/lsg_match.o: src/lsg_match.h src/lsg_match.c src/lsg_config.h src/lsg_data.h src/lsg_ref.h

src/lsg_ref.o: src/lsg_ref.h src/lsg_ref.c src/intset.h src/lsg_data.h

src/lsg_render.o: src/lsg_render.h src/lsg_render.c src/lsg_config.h src/lsg_data.h src/lsg_match.h src/lsg_ref.h

src/insetset.o: src/intset.h src/insetset.c

src/strutil.o: src/strutil.h src/strutil.c

data/lsg_data.o: src/lsg_data.h data/lsg_data.c

data/lsg_data.c: data/lsg.tsv data/generate.awk src/lsg_data.h
	awk -f data/generate.awk $< > $@

.PHONY: clean
clean:
	rm -rf $(OBJS) lsg
