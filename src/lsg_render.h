#pragma once

#include "lsg_config.h"
#include "lsg_ref.h"

int
lsg_render(const lsg_ref *ref, const lsg_config *config);
