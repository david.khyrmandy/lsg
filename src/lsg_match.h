#pragma once

#include "lsg_config.h"
#include "lsg_ref.h"

typedef struct {
    int start;
    int end;
} lsg_range;

typedef struct {
    int current;
    int next_match;
    lsg_range matches[2];
} lsg_next_data;

int
lsg_next_verse(const lsg_ref *ref, const lsg_config *config, lsg_next_data *next);
