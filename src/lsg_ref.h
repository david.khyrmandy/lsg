#pragma once

#include <regex.h>

#include "intset.h"

#define lsg_REF_SEARCH 1
#define lsg_REF_EXACT 2
#define lsg_REF_EXACT_SET 3
#define lsg_REF_RANGE 4
#define lsg_REF_RANGE_EXT 5

typedef struct lsg_ref {
    int type;
    unsigned int book;
    unsigned int chapter;
    unsigned int chapter_end;
    unsigned int verse;
    unsigned int verse_end;
    intset *verse_set;
    char *search_str;
    regex_t search;
} lsg_ref;

lsg_ref *
lsg_newref();

void
lsg_freeref(lsg_ref *ref);

int
lsg_parseref(lsg_ref *ref, const char *ref_str);
