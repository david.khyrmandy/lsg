#pragma once

typedef struct {
    int number;
    char *name;
    char *abbr;
} lsg_book;

typedef struct {
    int book;
    int chapter;
    int verse;
    char *text;
} lsg_verse;

extern lsg_verse lsg_verses[];

extern int lsg_verses_length;

extern lsg_book lsg_books[];

extern int lsg_books_length;
