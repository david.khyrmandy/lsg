# lsg

Lisez la parole de Dieu depuis votre terminal, désormais en Francais !
Embranchement de [kjv de bontibon](https://github.com/bontibon/kjv.git).
Lsg utilise la Sainte Bible par Louis Segond 1910 (LSG).

## Utilisation

	Utilisation : lsg [paramètres] [références...]

	Paramètres :
		-A [nombre]	Affiche [nombre] de versets après le verset correspondant au résultat de votre recherche.
		-B [nombre]	Affiche [nombre] de versets avant le verset correspondant au résultat de votre recherche.
		-C		Affiche le chapitre complet correspondant au résultat de votre recherche.
		-e		Met en avant les numéros de chapitres et de versets.
				(Comportement par défaut quand la sortie est un terminal.)
		-p		Redirige vers `less` avec le regroupement de chapitres, l'espacement, l'indentation et
				le retour à la ligne.
				(Comportement par défaut quand la sortie est un terminal.)
		-l		Liste les livres.
		-h		Affiche l'aide.

	Références :
		<Livre>
			Livre spécifique
		<Livre>:<Chapitre>
			Chapitre spécifique d'un livre
		<Livre>:<Chapitre>:<Verset>[,<Verset>]...
			Verset(s) spécifique(s) d'un chapitre d'un livre
		<Livre>:<Chapitre>-<Chapitre>
			Gamme de chapitres dans un livre
		<Livre>:<Chapitre>:<Verset>-<Verset>
			Gamme de versets dans un chapitre.
		<Livre>:<Chapitre>:<Verset>-Chapitre>:<Verset>
			Gamme de chapitres et de versets dans un livre.
		/<Recherche>
			Affiche tous les versets correspondants à votre recherche.
		<Livre>/<Recherche>
			Affiche toutes les versets correspondants à votre recherche dans un livre spécifique.

## Installation

- Sur `Ubuntu` et dérivés, téléchargez le paquet `libreadline-dev`. 

	```
	sudo apt install libreadline-dev
	```

- Clonez le dépôt git.

	```
	git clone https://gitlab.com/david.khyrmandy/lsg.git
	```

- Installez lsg.

	```	
	cd lsg
	make
	```

- Vous pouvez ajouter l'exécutable à votre `Path`, dans `/usr/local/bin`, par exemple.

## Licence

Domaine public.
